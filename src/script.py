from __future__ import print_function
import sys, re, os

def main():

    patternsList = ["it's", "its", "you're", "your", "they're", "their", "loose", "lose", "to", "too"]

     
    inFile = sys.argv[1]

    inF = open(inFile, 'r')

    #outF = open('onlyThose.txt', 'w')  only those lines containing error Patterns modified

    outOrig = open('originalFile.txt','w')

    outNew = open('newFile.txt','w')

    outKeepTrack = open('keepingTrackFile.txt','w')


    

    lineNo = 1
    for eachLine in inF:

        #regEx = r'\b' + eachPattern + r'\b'

        #strings = re.findall(regEx, eachLine.lower())

        aString = eachLine.lower()

        itsTuplesList = [(a.start(), a.end()) for a in list(re.finditer(r"\bit's\b", aString))]
        for eachTuple in itsTuplesList:     #replace and write to file for each tuple
            print(aString, file=outOrig, end='')
            
            writeLineFile = aString[0:eachTuple[0]] + "its" + aString[eachTuple[1]:]
            print(writeLineFile, file=outNew, end='')
            print(lineNo, eachTuple[0], eachTuple[1], file=outKeepTrack)

        its1TuplesList = [(a.start(), a.end()) for a in list(re.finditer(r"\bitis\b", aString))]
        for eachTuple in its1TuplesList:
            print(aString, file=outOrig, end='')
            
            writeLineFile = aString[0:eachTuple[0]] + "it's" + aString[eachTuple[1]:]
            print(writeLineFile, file=outNew, end='')
            print(lineNo, eachTuple[0], eachTuple[1], file=outKeepTrack)

        theyTuplesList = [(a.start(), a.end()) for a in list(re.finditer(r"\btheir\b", aString))]
        for eachTuple in theyTuplesList:
            print(aString, file=outOrig, end='')
            
            writeLineFile = aString[0:eachTuple[0]] + "they're" + aString[eachTuple[1]:]
            print(writeLineFile, file=outNew, end='')                
            print(lineNo, eachTuple[0], eachTuple[1], file=outKeepTrack)


        they1TuplesList = [(a.start(), a.end()) for a in list(re.finditer(r"\bthey're\b", aString))]
        for eachTuple in they1TuplesList:
            print(aString, file=outOrig, end='')
            
            writeLineFile = aString[0:eachTuple[0]] + "their" + aString[eachTuple[1]:]
            print(writeLineFile, file=outNew, end='')                
            print(lineNo, eachTuple[0], eachTuple[1], file=outKeepTrack)


        youTuplesList = [(a.start(), a.end()) for a in list(re.finditer(r"\byour\b", aString))]
        for eachTuple in youTuplesList:
            print(aString, file=outOrig, end='')

            writeLineFile = aString[0:eachTuple[0]] + "you're" + aString[eachTuple[1]:]
            print(writeLineFile, file=outNew, end='')                
            print(lineNo, eachTuple[0], eachTuple[1], file=outKeepTrack)


        you1TuplesList = [(a.start(), a.end()) for a in list(re.finditer(r"\byou're\b", aString))]
        for eachTuple in you1TuplesList:
            print(aString, file=outOrig, end='')

            writeLineFile = aString[0:eachTuple[0]] + "your" + aString[eachTuple[1]:]
            print(writeLineFile, file=outNew, end='')          
            print(lineNo, eachTuple[0], eachTuple[1], file=outKeepTrack)      


        toTuplesList = [(a.start(), a.end()) for a in list(re.finditer(r"\bto\b", aString))]
        for eachTuple in toTuplesList:
            print(aString, file=outOrig, end='')

            writeLineFile = aString[0:eachTuple[0]] + "too" + aString[eachTuple[1]:]
            print(writeLineFile, file=outNew, end='')          
            print(lineNo, eachTuple[0], eachTuple[1], file=outKeepTrack)      


        to1TuplesList = [(a.start(), a.end()) for a in list(re.finditer(r"\btoo\b", aString))]
        for eachTuple in to1TuplesList:
            print(aString, file=outOrig, end='')

            writeLineFile = aString[0:eachTuple[0]] + "to" + aString[eachTuple[1]:]
            print(writeLineFile, file=outNew, end='')          
            print(lineNo, eachTuple[0], eachTuple[1], file=outKeepTrack)      


        lineNo += 1 


                       

    outOrig.close()
    outNew.close()
    outKeepTrack.close()
    inF.close()




if __name__ == "__main__":
    main()
    

