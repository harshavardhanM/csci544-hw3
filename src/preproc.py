from __future__ import print_function
import sys, re, os

def main():

    inFile = sys.argv[1]

    inF = open(inFile, 'r')

    outF = open('preProcessTrain.txt', 'w')

                                            #language model context =>  They are stopping over at LAX. 
                                            #On their way to London, they are stopping over at LAX.
    

    for eachLine in inF:

        preProcessing = eachLine.lower()
        
        scraped = re.sub(r'["]+', '', preProcessing)

        scraped2 = re.sub(r"\bit is\b", "it's", scraped)

        scraped3 = re.sub(r"\bthey are\b", "they're", scraped2)

        scraped4 = re.sub(r"\byou are\b", "you're", scraped3)

        print(scraped4, file=outF, end='')

    
    outF.close()

    inF.close()
 



if __name__ == "__main__":
    main()
    

