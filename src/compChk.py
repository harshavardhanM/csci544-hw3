from __future__ import print_function
from itertools import izip
import sys, re, os

def main():

    origF = open('orig.out', 'r')

    newF = open('new.out', 'r')

    inRef = open('keepingTrackFile.txt', 'r')

    testF = open(sys.argv[1], 'r')

    outTest = open('correctOP.out', 'w')


    linesChangeList = []

    for eachLine in inRef:
        origValue = float(origF.readline().split()[3])
        newValue = float(newF.readline().split()[3])

        if newValue > origValue:
            linesChangeList.append(eachLine.split())

    #print(linesChangeList)

    inRef.close()
    origF.close()
    newF.close()


    testLineNo = 1

    listIter = 0

    for eachLine in testF:

        if listIter < len(linesChangeList):

            while testLineNo == int(linesChangeList[listIter][0]):

        
                begPos = int(linesChangeList[listIter][1])
                endPos = int(linesChangeList[listIter][2])

                wordToChange = eachLine[begPos:endPos]

                newWord = ""

                if wordToChange.lower() == "it's":
                    if wordToChange[0] == "I":
                        newWord = "Its"
                    else:
                        newWord = "its"

                elif wordToChange.lower() == "its":
                    if wordToChange[0] == "I":
                        newWord = "It's"
                    else:
                        newWord = "it's"

                elif wordToChange.lower() == "they're":
                    if wordToChange[0] == "T":
                        newWord = "Their"
                    else:
                        newWord = "their"

                elif wordToChange.lower() == "their":
                    if wordToChange[0] == "T":
                        newWord = "They're"
                    else:
                        newWord = "they're"

                elif wordToChange.lower() == "you're":
                    if wordToChange[0] == "Y":
                        newWord = "Your"
                    else:
                        newWord = "your"

                elif wordToChange.lower() == "your":
                    if wordToChange[0] == "Y":
                        newWord = "You're"
                    else:
                        newWord = "you're"

                elif wordToChange.lower() == "too":
                    if wordToChange[0] == "T":
                        newWord = "To"
                    else:
                        newWord = "to"

                elif wordToChange.lower() == "to":
                    if wordToChange[0] == "T":
                        newWord = "Too"
                    else:
                        newWord = "too"

                elif wordToChange.lower() == "loose":
                    if wordToChange[0] == "L":
                        newWord = "Lose"
                    else:
                        newWord = "lose"

                elif wordToChange.lower() == "lose":
                    if wordToChange[0] == "L":
                        newWord = "Loose"
                    else:
                        newWord = "loose"
                

                eachLine = eachLine[0:begPos] + newWord + eachLine[endPos:]

                #print(newLine, file=outTest, end='')

                listIter += 1
                if listIter == len(linesChangeList):
                    break;

        print(eachLine, file=outTest, end='')

        
        testLineNo += 1

    testF.close()

    outTest.close()



   




if __name__ == "__main__":
    main()
    

