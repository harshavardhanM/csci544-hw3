

# README #

Harshavardhan Manjunatha

I employed the SRI Language modeler. I utilized a tri-gram feature Language model. I used the training Data from the Kaggle billion word imputation challenge. I pre-processed the training Data to lowercase but made sure when correcting the Test Data or Development Data to maintain the case.

Runs on python2


### What is this repository for? ###

1. After pre-processing the dataset with preproc.py, feed the pre-processed training dataset to SRILM to create a tri-gram language model

        % ngram-count -order 3 -text PreprocessTrain.txt -lm newLangModel.out

    Building a language model of tri-grams based using the Preprocessed Training Data 
    

    Algorithm/Logic - I generate three files by running "script.py" on the Development/Test Data

        % python2 src/script.py hw3/hw3.dev.err.txt

    1. originalFile.txt, which is a copy of the Development/Test Data
    2. newfile.txt, with the opposites of the homophones replaced inline
    3. keepingTrackFile.txt To keep track of changes i.e store the line number of the Test/Dev file where the change needs to be done and store the position of the homophone word that needs to be or need not be corrected

    

        % ngram -ppl originalFile.txt -order 3 -lm newLangModel.out -debug 1 | grep "logprob=" > orig.out

        % ngram -ppl newFile.txt -order 3 -lm newLangModel.out -debug 1 | grep "logprob=" > new.out

    Running the originalFile and newFile with the Language modeler yields the log probabilities and perplexities.

    Comparing each original Sentence in originalFile against itself with the homophone correction in place performed in the newFile, I choose the sentence with the higher probability(or effecitively lower perplexity/error) as the sentence to be stored and corrected later while reading the Test file again for in place correction

    orig.out & new.out contain the corresponding log probabilities for each line of the corresponding files


        % python2 ../src/compChk.py hw3.dev.err.txt

    The above command/script generates the in-place homophone corrected final output file, after choosing the line with the higher probability
